"use strict";

let counterLink = document.querySelector('#counter-link');
let counterContainer = document.querySelector('#counter-container');
let galleryLink = document.querySelector('#gallery-link');
let galleryContainer = document.querySelector('#gallery-container');

let target = document.querySelector('#gettext');
let output = document.querySelector('#settext');
let count = document.querySelector('#count-letters');
let reset = document.querySelector('#reset');


let initializeEventListener = () => {
    
    counterLink.addEventListener('click', (event) => {
        event.preventDefault();
        changeCounterVisible();
    });

    galleryLink.addEventListener('click', (event) => {
        event.preventDefault();
        changeGalleryVisible();
    });

    reset.addEventListener("click", (event) => {
        event.preventDefault();
        target.disabled = false;
        target.value = "";
        target.focus();
        output.value = "";
        count.style.color = "inherit";
        count.textContent = "0";
    });
};

let changeCounterVisible = () => {
    counterContainer.style.display = "block";
    galleryContainer.style.display = "none";
};

let changeGalleryVisible = () => {
    galleryContainer.style.display = "block";
    counterContainer.style.display = "none";
};

let countsSymbol = (text) => {
    let textCounts = text.length;
    count.textContent = textCounts;
    if (textCounts >= 199) {
        target.disabled = true;
        count.style.color = "red";
    }
};

function handle(event) {
    let letterSymbol = String.fromCharCode(event.charCode);
    countsSymbol(output.value += letterSymbol);
}

let initializeState = () => {
    window.onload = target.focus();
    target.addEventListener('change', (event) => {
        handle(event);
    });
    target.addEventListener('keypress', (event) => {
        handle(event);
    });
    initializeEventListener();
};

initializeState();
