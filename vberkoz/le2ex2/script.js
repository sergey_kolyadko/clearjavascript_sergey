//Lesson 2, exercise 2 homework
//
//Declare 3 variables, assign to them the same value and output to the console.
//Assign each variable a different value and output to the console.
var var1 = 0,
    var2 = 0,
    var3 = 0;

console.log(var1 + " " + var2 + " " + var3);

var1 = 1;
var2 = 2;
var3 = 3;

console.log(var1 + " " + var2 + " " + var3);