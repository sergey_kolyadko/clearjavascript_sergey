
function FormValidator(form) {
	
	var DOMElements = null;
	var accountArray = [];
    setFormData(form);
	
	function toggleErrorMsg(){
		DOMElements.bgDanger.classList.toggle('hidden');
	}

	function toggleTable(){
		DOMElements.formFillResult.classList.toggle('hidden');
	}

	function toggleForm(){
		DOMElements.formArea.classList.toggle('hidden');
	}

	function addNewRow() {
		var password = DOMElements.password.value;
		accountArray.push(password);
		password = makeStars.call(this, password);
		DOMElements.tableContent.innerHTML += `<tr>
			<th scope="row" class="text-center">${accountArray.length}</th>
			<td>${DOMElements.name.value}</td>
			<td>${DOMElements.email.value}</td>
			<td>
				<span>${password}</span>
				<button class="btn btn-default pull-right" password-status="0">Показать пароль</button>
			</td>
		</tr>`
	}

	function verifyEmail(email) {
		var emailPattern = /\S+@\S+\.\S+/;
		return emailPattern.test(email);
	}

	function validate(event) {
		event.preventDefault();
		if(DOMElements.name.value && verifyEmail.call(this, DOMElements.email.value) && DOMElements.password.value) {
				addNewRow.call(this);
				if(!DOMElements.bgDanger.classList.contains('hidden')){toggleErrorMsg.call(this)}
				toggleForm.call(this);
				toggleTable.call(this);
		} else {
				if(DOMElements.bgDanger.classList.contains('hidden')){toggleErrorMsg.call(this)}
		}
	}

	function makeStars(value) {
		return value.replace(/[\w \W]/g, '*');
	}

	function togglePasswordVisibility(event) {
		var target				 = event.target;
		var togglePasswordStatus = target.getAttribute("password-status");
		var password			 = target.previousElementSibling;
		var index				 = target.parentNode.parentNode.childNodes[1].innerText;
		togglePasswordStatus --;
		togglePasswordStatus = Math.abs(togglePasswordStatus);
		target.setAttribute("password-status", togglePasswordStatus);
		if (togglePasswordStatus == 0) {
			password.innerText = makeStars.call(this, accountArray[index - 1]);
			target.innerHTML = "Показать пароль";
		} else {
			password.innerText = accountArray[index - 1];
			target.innerHTML = "Скрыть пароль";
		}
	}

	function addNewAccount() {
		toggleTable.call(this);
		toggleForm.call(this);
	}

	function initListeners() {
		DOMElements.submitBtn.addEventListener("click", validate.bind(this));
		DOMElements.resetBtn.addEventListener("click", addNewAccount.bind(this));
		DOMElements.tableContent.addEventListener("click", togglePasswordVisibility.bind(this));
	}

	function setFormData(form) {	
		DOMElements = form;
	}

	this.initValidator = function(form) {
		initListeners.call(this);
	}

};

var formValidator = new FormValidator({
	name				: document.querySelector("#inputName"),
	email				: document.querySelector("#inputEmail"),
	password			: document.querySelector("#inputPassword"),
	tableContent		: document.querySelector("#table-content"),
	submitBtn			: document.querySelector("#submit"),
	resetBtn			: document.querySelector("#reset"),
	formFillResult		: document.querySelector("#form-fill-result"),
	formArea			: document.querySelector(".form-area"),
	bgSuccess			: document.querySelector(".bg-success"),
	bgDanger			: document.querySelector(".bg-danger")
})
formValidator.initValidator();