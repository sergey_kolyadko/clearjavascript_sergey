(function() {
    function inheritense(parent, child) {
        var tempChild = child.prototype;

        if (Object.create) {
            child.prototype = Object.create(parent.prototype);
        } else {
            function F() {};
            F.prototype = parent.prototype;
            child.prototype = new F();
        }

        for (var key in tempChild) {
            if (tempChild.hasOwnProperty(key)) {
                child.prototype[key] = tempChild[key];
            }
        }
    }

    function Filter(elements) {
        this.DOMElements = {
            price: document.querySelector("#priceFilter"),
            date: document.querySelector("#dateFilter"),
            result: document.querySelector("#result"),
            btnFilter: document.querySelector(".btn")
        };
        for (var key in elements) {
            this.DOMElements[key] = elements[key];
        }
        this.data = {};
    }

    Filter.prototype = {
            initListeners: function() {
                this.DOMElements.btnFilter.addEventListener("click", this.getFullFilter.bind(this));
            },
            getPriceFilter: function() {
                this.data.price = this.DOMElements.price.value;
            },

            getDateFilter: function() {

                this.data.date = this.DOMElements.date.value;
            },

            printFilterValues: function() {
                this.DOMElements.result.innerHTML = JSON.stringify(this.data);
                console.log(this.data);
            },

            getFullFilter: function() {
                this.getPriceFilter();
                this.getDateFilter();
                this.printFilterValues();
            }
        }
        //-----------------------------------------------------------------------
    function ClockFilter(elements) {
        Filter.apply(this, arguments);
    }

    ClockFilter.prototype = {
        getGenderFilter: function() {
            this.data.gender = this.DOMElements.genderFilter.value;
        },
        getStyleClock: function() {
            var elements = this.DOMElements.styleWatch; 
            for (var i = 0; i < elements.length; i++) {
                if (elements[i].checked) {
                    this.data.style = elements[i].value;
                }
            }
        },
        getFullFilter: function() {
            this.getGenderFilter();
            this.getStyleClock();
            Filter.prototype.getFullFilter.call(this);
        }
    }
    //-----------------------------------------------------------------------
    function CarFilter(elements) {
        Filter.apply(this, arguments);
    }
    CarFilter.prototype = {
        getCarYear: function() {
            var year = this.DOMElements.carYear.value
            if ((!isNaN(parseFloat(year)) && isFinite(year))) {
                this.data.carYear = year;
            } else {
                delete this.data.carYear;
            }
        },
        getCarStatus: function() {
            var status = this.DOMElements.carStatus
            if (status.checked) {
                this.data.carStatus = status.value;
            } else if (status) {
                delete this.data.carStatus
            }
        },
        getCarMileage: function() {
            var elements = this.DOMElements.carDistance;
            var distanceArr = [];
            for (var i = 0; i < elements.length; i++) {
                if (elements[i].checked) {
                    distanceArr.push(elements[i].value);
                }
            }
            if (distanceArr.length == 0) {
                delete this.data.carDistance;
            } else {
                this.data.carDistance = distanceArr;
            }
        },
        getFullFilter: function() {
            this.getCarYear();
            this.getCarStatus();
            this.getCarMileage();
            Filter.prototype.getFullFilter.call(this);
        }
    }

    if (document.querySelector(".filters").getAttribute("data-name") == "clockFilter") {
        inheritense(Filter, ClockFilter);

        var clockFilter = new ClockFilter({
                                            genderFilter: genderFilter = document.querySelector("#genderFilter"),
                                            styleWatch : styleWatch = document.getElementsByName("watch")
                                        });
        clockFilter.initListeners();
    } else {
        inheritense(Filter, CarFilter);
        var carFilter = new CarFilter({
                                        carYear: carYear = document.querySelector("#carYear"),
                                        carStatus: carStatus = document.querySelector("#carStatus"),
                                        carDistance: carDistance = document.getElementsByName("distance")
                                    });
        carFilter.initListeners();
    }
})();
