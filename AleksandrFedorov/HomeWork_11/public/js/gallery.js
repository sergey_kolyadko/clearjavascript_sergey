'use strict';

var gallery = (function() {
    var sourceArr = [];
    var workArr = [];
    var addedElements = document.getElementById("count");
    var container = document.getElementById("result");
    var addBtnGallery = document.getElementById("add");
    var saveGalleryBtn = document.getElementById("save");
    var successMsg = document.querySelector(".bg-success");

    function getData() {
        $.get("js/data.json", function(data) {
            sourceArr = getChangedData(data);
        });
    }
    //-----------------work with data begin-----------------
    function changeName(name) {
        return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
    }

    function modifyUrl(url) {
        return "http://" + url;
    }

    function modifyDescription(description) {
        return description.substr(0, 15) + "...";
    }

    function modifyDate(date) {
        var tmpDate = new Date(date);
        var year = tmpDate.getFullYear(),
            month = tmpDate.getMonth() + 1,
            day = tmpDate.getDate(),
            hours = tmpDate.getHours(),
            minutes = tmpDate.getMinutes();
        var addZero = function(partDate) {
            if (partDate < 10) {
                return "0" + partDate;
            } else {
                return partDate;
            }
        }
        month = addZero(month);
        day = addZero(day);
        hours = addZero(hours);
        minutes = addZero(minutes);
        return `${year}/${month}/${day} ${hours}:${minutes}`
    }

    function getChangedData(workArr) {
        return workArr.map(function(element, index) {
            return {
                name: changeName(element.name),
                url: modifyUrl(element.url),
                id: element.id,
                description: modifyDescription(element.description),
                date: modifyDate(element.date),
                params: `${element.params.status}=>${element.params.progress}`
            }
        });
    }
    //-----------------work with data end-----------------

    //-----------------work with gallery begin-----------------
    function addElementInGallery() {
        // var addedElem;
        if (sourceArr.length != 0) {
            insertGalleryElement(moveElementSourceToWorkArr(), container);
        } else {
            alert("Sorry, no more elements.");
            return false;
        }
        showCountInGallery(container);
    };

    function moveElementSourceToWorkArr() {
        var addedElem = sourceArr.shift();
        workArr.push(addedElem);
        return addedElem;
    }

    function insertGalleryElement(item, container) {
        var itemTemplate = `<div class="col-sm-3 col-xs-6 text-center">\
                <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">${item.name}</div>\
                    <div class="text-muted hidden">${item.id}</div>
                    <div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
                    <div class="text-muted">${item.date}</div>\
                </div>\
                <button class="btn btn-danger" data-id="${item.id}">Удалить</button>
            </div>`;
        container.innerHTML += itemTemplate;
    }

    function showCountInGallery(container) {
        addedElements.innerHTML = workArr.length;
    }

    function removeElementOutGallery(event) {
        var target = event.target;
        var removeElement = target.getAttribute("data-id");
        container.removeChild(target.closest('div'));
        moveElementWorkArrToSourceArr(removeElement);
        showCountInGallery(container);
    }

    function moveElementWorkArrToSourceArr(dataId) {
        workArr.map(function(item, index) {
            if (item["id"] == dataId) {
                var removed = workArr.splice(index, 1);
                sourceArr.push(removed[0]);
            }
        })
    }

    function showSuccessMsg() {
        successMsg.classList.remove("hide");
    }

    function saveGallery(event) {

        $.post("/api/save", { gallery: workArr }, function(data) {
            if (data == "ok") {
                showSuccessMsg();
            }
        })
    }

    function initListeners () {
        addBtnGallery.addEventListener("click", addElementInGallery);
        container.addEventListener("click", removeElementOutGallery);
        saveGalleryBtn.addEventListener("click", saveGallery);
    }
    //-----------------work with gallery end-----------------
    return {
        initListeners : function () {
             initListeners();
        },
        init : function () {
             getData();
        }
    }


})();
gallery.init();
gallery.initListeners();
