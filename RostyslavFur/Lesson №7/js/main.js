var first = document.querySelector('#first-line');
var second = document.querySelector('#second-line');
var third = document.querySelector('#third-line');
var modifyName = (name) => {
	return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
};
var modifyUrl = (url) => {
	return 'http://' + url; 
};
var modifyDescript = (descript) => {
	return descript.substr(0, 15) + '...';
}; 
var modifyDate = (date) => {
        var tmpDate = new Date(date);
        var fixedDate = {
			year:    tmpDate.getFullYear(),
            month:   tmpDate.getMonth()+1,
            day:     tmpDate.getDate(),
            hours:   tmpDate.getHours(), 
            minutes: tmpDate.getMinutes(),
		};
		for (var key in fixedDate){
			fixedDate[key] = fixedDate[key] < 10 ? '0' + fixedDate[key] : fixedDate[key];
		}
		return fixedDate.year + '/' + fixedDate.month + '/' + fixedDate.day + ' ' + fixedDate.hours + ':' + fixedDate.minutes;
};
var modifyParams = (params) => {
	return params.status + ' => ' + params.progress;
};
var modifyData = (newData) => {
	return newData.map(function(item,index){
      return{
		name: modifyName(item.name),
        url: modifyUrl(item.url),
		description: modifyDescript(item.description),
		date: modifyDate(item.date),
		params: modifyParams(item.params)
      };
	});
};
var applyReplace = (item, divFirst) => {
var resultHTML = "";
		var itemTemplate = '<div class="col-sm-3 col-xs-6">\
				<img src="$url" alt="$name" class="img-thumbnail">\
				<div class="info-wrapper">\
					<div class="text-muted">$name</div>\
					<div class="text-muted">$description</div>\
                    <div class="text-muted">$params</div>\
					<div class="text-muted">$date</div>\
				</div>\
	</div>';
resultHTML = itemTemplate
	.replace(/\$name/gi, item.name)
	.replace("$url", item.url)
    .replace("$params", item.params)
	.replace("$description", item.description)
	.replace("$date", item.date);
divFirst.innerHTML += resultHTML;
};
var applyInterpolation = (item, divSecond) => {
var itemTemplate = `<div class="col-sm-3 col-xs-6">\
				<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
				<div class="info-wrapper">\
					<div class="text-muted">${item.name}</div>\
					<div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
					<div class="text-muted">${item.date}</div>\
				</div>\
			</div>`; 
divSecond.innerHTML += itemTemplate;		 
};
var applyCreateElement = (item, divThird) => {
	var newElement = document.createElement ('div');
	newElement.className = "col-sm-3 col-xs-6";
	divThird.appendChild(newElement);
	
	var newImg = document.createElement('img');
	newImg.src = item.url;
	newImg.alt = item.name;
	newImg.className = 'img-thumbnail';
	newElement.appendChild(newImg);
   
    var newDiv = document.createElement('div');
    newDiv.className = 'info-wrapper';
    newElement.appendChild(newDiv);	
	
	var mainNewDivEl = document.createElement ('div');
	mainNewDivEl.className = 'text-muted';
	mainNewDivEl.innerHTML = item.name;
	newDiv.appendChild(mainNewDivEl);
var cloneDiv = (itemVal, div) => {
		var newDivEl = div.cloneNode(true);
		newDivEl.innerHTML = itemVal;
		newDiv.appendChild(newDivEl);
	}
cloneDiv(item.description, mainNewDivEl);
cloneDiv(item.params, mainNewDivEl);
cloneDiv(item.date, mainNewDivEl);
}
var transform = () => {
	var modifiedData = modifyData(data);
modifiedData.forEach (function(item, index) {
	if (index <= 2){
		applyReplace(modifiedData[index], first);
	}
	else if (index <= 5){
		applyInterpolation(modifiedData[index], second);
	}
	else if (index <= 8) {
		applyCreateElement(modifiedData[index], third)
	}
})
};	
transform();