'use strict';
var formValidator = (function(){
	
	var DOMElements = null;
	var userData = [];
	function showDiv(form) {
		form.style.display = 'block';
	}
	function hideDiv(form){
		form.style.display = 'none';
	}
	function checkEmail(email){
		var test = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
		return email.match(test)
	}
	function resetForm(){
		DOMElements.name.value = "";
		DOMElements.email.value = "";
		DOMElements.password.value = "";
		showDiv(DOMElements.formDiv);
		hideDiv(DOMElements.table);
	}
	function showPassw(event){
		var click = event.target;
		var passwordDiv = click.parentNode.previousElementSibling;
		var id = (click.getAttribute('data-id')-1);
		var display = click.getAttribute('password-value');
		if (display === 'hide'){
			passwordDiv.innerHTML = userData[id].passwVal;
			click.innerHTML = 'Скрыть пароль';
			click.setAttribute('password-value', 'show');
		}
		else {
			passwordDiv.innerHTML = userData[id].passwHidden;
			click.innerHTML = 'Показать пароль';
			click.setAttribute('password-value', 'hide');
		}
	}
	function createData (){
		var dataLength = userData.push({
			nameVal: DOMElements.name.value,
			emailVal: checkEmail(DOMElements.email.value),
			passwHidden: (DOMElements.password.value).replace(/./gi, '*'),
			passwVal: DOMElements.password.value,
			rowNumber: userData.length + 1
		});
		addNewTableLine(userData[dataLength - 1]);
	}
	var addNewTableLine = function({nameVal, emailVal, passwHidden, rowNumber}) {
		var row =   `<tr>\
					<th id="count" scope="row">${rowNumber}</th>\
					<td>${nameVal}</td>\
					<td>${emailVal}</td>\
					<td>${passwHidden}</td>\
					<td><button class="btn btn-default" data-id="${rowNumber}" password-value="hide" >Показать пароль</button></td>\
					</tr>`; 
		DOMElements.tableContent.innerHTML += row;
	}
	function validate (event) {
		if(DOMElements.name.value 
			&& checkEmail(DOMElements.email.value) 
			&& DOMElements.password.value) {
				showDiv(DOMElements.successDiv);
				showDiv(DOMElements.table);
				hideDiv(DOMElements.formDiv);
				hideDiv(DOMElements.errorDiv);
				createData();
				event.preventDefault();
		} 
		else {
			showDiv(DOMElements.errorDiv);
			event.preventDefault();
		}
	}
	function initListeners() {
		DOMElements.submitBtn.addEventListener("click", validate);	
		DOMElements.resetBtn.addEventListener("click", resetForm);
		DOMElements.tableContent.addEventListener("click", showPassw);
	}
	
	return {
		setFormData : function(form){	
			DOMElements = form;
		},
		initValidator: function(){
			initListeners();
		}
	}
	
}())
formValidator.setFormData({
	name 	: document.querySelector("#inputName"),
	email 	: document.querySelector("#inputEmail"),
	password : document.querySelector("#inputPassword"),
	formDiv: document.querySelector("#info"),
	tableContent : document.querySelector("#table-content"),
	table: document.querySelector("#table"),
	submitBtn: document.querySelector("#submit"),
	resetBtn: document.querySelector("#reset"),
	successDiv: document.querySelector("#success"),
	errorDiv: document.querySelector("#error"),
})
formValidator.initValidator();