var myVar = 100;

//to string
var convertVar = String(myVar);
console.log (convertVar); // "100"

alert (myVar); // "100"

var currency = "$";
console.log(myVar+currency); // "100$"

//to number
convertVar = Number(myVar);
console.log (convertVar); // 100

convertVar = +myVar;
console.log (convertVar); // 100

convertVar = parseInt(myVar);
console.log (convertVar); // 100

//to boolean
convertVar = Boolean(myVar);
console.log (convertVar); // true

convertVar = !!myVar;
console.log (convertVar); // true

convertVar = !myVar;
console.log (convertVar); // false

//to object
convertVar = Object(myVar);
console.log (convertVar); // Number {[[PrimitiveValue]]: 100}

//to null
myVar = null;
console.log (myVar); // null


