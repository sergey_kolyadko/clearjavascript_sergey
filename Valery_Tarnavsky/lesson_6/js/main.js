var btn = document.getElementById("play");

function createNewArray(arr, count){
    var newArr = [];
    arr.forEach(function(item, index){
        if(count > index){
            newArr.push({
                url: item.url,
                name: item.name,
                params: item.params,
                description: item.description,
                date: item.date
            })
        }
    });
    return newArr;
}

function capitalizeFirstLetter(s) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
}

function cutString (s){
    return s.slice(0, 15) + "...";
}

function formatDate(date){
    var tmpDate = new Date(date);
    var fixDate = {
        year    : tmpDate.getFullYear(),
        month   : tmpDate.getMonth() + 1,
        day     : tmpDate.getDate(),
        hours   : tmpDate.getHours(),
        minutes : tmpDate.getMinutes()
};
    for (var k in fixDate) {
        fixDate[k] = fixDate[k] < 10  ? ('0'+ fixDate[k]) : fixDate[k];
    }
    return fixDate.year + "/" + fixDate.month + "/" + fixDate.day + " " +  fixDate.hours + ":" + fixDate.minutes;
}

function print(arr){
    console.log(arr);
}

function transformArray (arr){
    return arr.map(function(item) {
        return {
            url: "http://" + item.url,
            name: capitalizeFirstLetter(item.name),
            params: item.params.status + "=>" + item.params.progress,
            description: cutString(item.description),
            date: formatDate(item.date)
        }
    });
}

function transform() {
    var count  = 5;
    print(transformArray(createNewArray(data, count)));
}

btn.addEventListener("click", transform);