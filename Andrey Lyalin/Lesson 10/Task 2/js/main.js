
var formValidator = new function () {
    var DOMElements = null;
    var index = 0;
    var msgError = document.getElementById("msgError").classList;
    var msgSuccess = document.getElementById("msgSuccess").classList;
    var informTable = document.querySelector("#data-table").classList;
    var btnHide = document.querySelector("#data-table");
    var formArr =[];

    showErrorMsg = function(){  //Собщение, неправильно заполнена форма
        msgError.add('bg-danger');
        msgError.remove('hide');
        if(!msgSuccess.contains('hide')){
            msgSuccess.add('hide');
        };
    };

    showSuccessMsg = function(){ //Сообщение, правильно заполнена форма
        msgSuccess.add('bg-success');
        msgSuccess.remove('hide');
        if(!msgError.contains('hide')){
            msgError.add('hide');
        };
    };

    this.inspectionMail = function(mail) { // Проверка почты
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(mail);
    };

    this.showHidePassword = function (inputPassword) { //Скрыть/показать пароль в форме
        event.preventDefault();
        var element = document.getElementById("inputPassword");
        var inp = document.createElement("input");
        if (element.type == 'password') {
            inp.id = "inputPassword";
            inp.type = "text";
            inp.value = element.value;
            inp.classList = element.classList;
            element.parentNode.replaceChild(inp, element);
        }
        else {
            inp.id = "inputPassword";
            inp.type = "password";
            inp.value = element.value;
            inp.classList = element.classList;
            element.parentNode.replaceChild(inp, element);
        }
    };

    this.showHidePasswordTable = function(event) { //Скрыть/Показать пароль в таблице
        var tr = event.target.closest("tr");
        var index = tr.getAttribute("data-index");
        var passwordCell = tr.getElementsByClassName("password-cell")[0];
        var passwordButton = tr.getElementsByClassName("show-password")[0];
        var passwordStatus = passwordButton.getAttribute("data-status");
        if(passwordStatus == 'hide'){
            passwordCell.innerHTML = formArr[index].password;
            passwordButton.innerHTML = "Спрятать";
            passwordButton.setAttribute("data-status", "show");
        }else if(passwordStatus == "show"){
            passwordCell.innerHTML = formArr[index].passwordStars;
            passwordButton.innerHTML = "Показать";
            passwordButton.setAttribute("data-status", "hide");
        }

    };

    this.toggleTableVisibility = function() {
        if(informTable.contains('hide')){
            informTable.remove('hide');
        }else if(!informTable.contains('hide')){
            informTable.add('hide');
            this.clearForm();
        }
    };

    this.clearForm = function() {
        DOMElements.name.value = "";
        DOMElements.email.value = "";
        DOMElements.password.value = "";
    };

    this.convertCharactersToAsteriks = function(string) {
        return string.replace(/[\w \W]/g, '*');
    };

    this.createArrayItem = function() {
        return {
            name: DOMElements.name.value,
            email: DOMElements.email.value,
            password:DOMElements.password.value,
            passwordStars:this.convertCharactersToAsteriks(DOMElements.password.value)
        };
    };

    this.addNewTableLine = function() {  //Заполнение таблицы формы данными
        var newItem = this.createArrayItem();
        formArr.push(newItem);

        var row = `<tr data-index="${index}">\
						<th scope="row">${index + 1}</th>\
						<td>${DOMElements.name.value}</td>\
                		<td>${DOMElements.email.value}</td>\
						<td class="password-cell">${this.convertCharactersToAsteriks(DOMElements.password.value)}
						</td>
						<td><button class="show-password" data-status="hide">Показать</button>\
						</td>\
					</tr>`;

        DOMElements.tableContent.innerHTML += row;
        this.toggleTableVisibility();// Показывает таблицу с данными
        index++;
    };

    this.validate = function (event) { //Проверка формы
        event.preventDefault();
        if(DOMElements.name.value
            && this.inspectionMail(DOMElements.email.value)
            && DOMElements.password.value) {
            showSuccessMsg(); //Сообщение, правильно заполнена форма
            this.addNewTableLine();

        } else {
            showErrorMsg();
        }
    };

    this.initListeners = function () {
        DOMElements.submitBtn.addEventListener("click", this.validate.bind(this));
        DOMElements.resetBtn.addEventListener("click", this.toggleTableVisibility.bind(this));
        DOMElements.hideOpenBtn.addEventListener("click", this.showHidePassword.bind(this));
        DOMElements.tableContent.addEventListener("click", this.showHidePasswordTable.bind(this));
    };

    setFormData = function(form){
        DOMElements = form;
    };
    this.initValidator = function(form){
        this.initListeners();
    };

    setFormData({
        name 	: document.querySelector("#inputName"),
        email 	: document.querySelector("#inputEmail"),
        password : document.querySelector("#inputPassword"),
        tableContent : document.querySelector("#table-content"),
        submitBtn: document.querySelector("#submit"),
        resetBtn: document.querySelector("#reset"),
        hideOpenBtn: document.querySelector("#btn_hide-open")
    });

};

formValidator.initValidator();