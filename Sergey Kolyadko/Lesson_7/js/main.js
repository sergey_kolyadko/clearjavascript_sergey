
function editUrl(link) {
	return "http://" + link;
}

function editName(nameCar){
	return nameCar.charAt(0).toUpperCase() + nameCar.substring(1).toLowerCase();
}

function editDesc(carDesc){
	return carDesc.slice(0,15) + "...";
}

function editDate(oldFormatDate) {
			var newFormatDate = new Date(oldFormatDate);
			var newDate = {
                   year: newFormatDate.getFullYear(),
				   month: newFormatDate.getMonth(),
				   day: newFormatDate.getDate(),
				   hours: newFormatDate.getHours(),
				   minutes: newFormatDate.getMinutes(),
           		};
               for ( var key in newDate){
                 if (newDate[key] < 10){
                     newDate[key] =  "0" + newDate[key];
                 } else {
                     newDate[key];
                 }
               }
          return newDate.year + "/" + newDate.month + "/" + newDate.day + " " + newDate.hours + ":" + newDate.minutes;
}

function editParams (carParam) {
	return carParam.status + "=>" + carParam.progress;
}

	
function editNewArr(newList){
	return newList.map(function(item){
		return {
				url:   		  editUrl(item.url),
                name:         editName(item.name),
				params:       editParams(item.params),
				description:  editDesc(item.description),
				date:         editDate(item.date)
		}
	})
}


function displayWithReplace(item){
	var resultHTML = "";
	var itemTemplate = '<div class="col-sm-3 col-xs-6">\
					<img src="$url" alt="$name" class="img-thumbnail">\
					<div class="info-wrapper">\
						<div class="text-muted">$name</div>\
						<div class="text-muted">$description</div>\
						<div class="text-muted">$params</div>\
						<div class="text-muted">$date</div>\
					</div>\
				</div>';

	resultHTML = itemTemplate
		.replace(/\$name/gi, item.name)
		.replace("$url", item.url)
		.replace("$params", item.params)
		.replace("$description", item.description)
		.replace("$date", item.date);
			
return resultHTML;		
}

function displayWithInterpolation(item){
	var resultHTML = "";
    var itemTemplate = `<div class="col-sm-3 col-xs-6">\
        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
            <div class="info-wrapper">\
                <div class="text-muted">${item.name}</div>\
                <div class="text-muted">${item.description}</div>\
                <div class="text-muted">${item.params}</div>\
                <div class="text-muted">${item.date}</div>\
            </div>\
        </div>`
	
	resultHTML += itemTemplate
		.replace("$number", item.id)
		.replace(/\$name/gi, item.name)
		.replace("$url", item.url)
		.replace("$params", item.params)
		.replace("$description", item.description)
		.replace("$date", item.date);
				
	return resultHTML;
}
/*
function createItems(items){
    var grid = document.createElement("div");
    grid.className = "col-sm-3 col-xs-6";

    var thumbnail = document.createElement("img");
    thumbnail.setAttribute("src", items.url);
    thumbnail.setAttribute("alt", items.name);
    thumbnail.className = "img-thumbnail";

    var info = document.createElement("div");
    info.className = "info-wrapper";

    var innerName = document.createElement("div");
    innerName.className = "text-muted";
    innerName.innerHTML =  items.name;
    innerParams = innerName.cloneNode(true);
    innerParams.innerHTML = items.params;
    innerDescription = innerName.cloneNode(true);
    innerDescription.innerHTML = items.description;
    innerDate = innerName.cloneNode(true);
    innerDate.innerHTML = items.date;

    info.appendChild(innerName);
    info.appendChild(innerDescription);
    info.appendChild(innerParams);
    info.appendChild(innerDate);
    grid.appendChild(thumbnail);
    grid.appendChild(info);

    return grid;
}
*/
function displayWithCreatElem(item){
	
	var mainBlock = document.createElement('div');
		mainBlock.className = "col-sm-3 col-xs-6";
	
	
	var imageCar = document.createElement('img');
		imageCar.setAttribute("src", item.url);
		imageCar.setAttribute("alt", item.name);
		imageCar.className = "img-thumbnail";
		mainBlock.appendChild(imageCar);

	var innerBlock = document.createElement('div');
		innerBlock.className = "info-wrapper";
		mainBlock.appendChild(innerBlock);

	var nameBlock = document.createElement('div');
		nameBlock.className = "text-muted";
		nameBlock.innerHTML = item.name;
		innerBlock.appendChild(nameBlock);
		
	var	descriptionBlock = nameBlock.cloneNode(true);
		descriptionBlock.innerHTML = item.description;
		innerBlock.appendChild(descriptionBlock);
		
	var	paramsBlock = nameBlock.cloneNode(true);
		paramsBlock.innerHTML = item.params;
		innerBlock.appendChild(paramsBlock);
		
	var	dateBlock = nameBlock.cloneNode(true);
		dateBlock.innerHTML = item.date;
		innerBlock.appendChild(dateBlock);
	
	return mainBlock;
}

var first = document.querySelector('#first-line');
var second = document.querySelector('#second-line');
var third = document.querySelector('#third-line');

var editedData = editNewArr(data);


function displayResult(method, elemId){
    elemId.innerHTML += method;
}

function createGallery (array){
	array.forEach(function(item, index){
		if (index < 3) {
			displayResult(displayWithReplace(item), first);
		} else if (index < 6) {
			displayResult(displayWithInterpolation(item), second);
		} else if (index < 9){
			displayResult(displayWithCreatElem(item).outerHTML, third);
		}
	});
}

createGallery (editedData);
