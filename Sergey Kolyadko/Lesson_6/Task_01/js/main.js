var btn = document.getElementById("play");

function leftId (mainArr, count) {
		var newArr = [];
	   mainArr.forEach(function(item, index){
        if(index < count){
            newArr.push({
				url:          item.url,
                name:         item.name,
				params:       item.params,
				description:  item.description,
				date:         item.date
            })
        }
    })
	return newArr;
}

function editUrl(link) {
	return "http://" + link;
}

function editName(nameCar){
	return nameCar.charAt(0).toUpperCase() + nameCar.substring(1).toLowerCase();
}

function editDesc(carDesc){
	if (carDesc.length > 15){
		return carDesc.slice(0,15) + "...";
	} else {
		return carDesc;
	}
}

function editDate(oldFormatDate) {
			var newFormatDate = new Date(oldFormatDate);
			var newDate = {
                   year: newFormatDate.getFullYear(),
				   month: newFormatDate.getMonth(),
				   day: newFormatDate.getDate(),
				   hours: newFormatDate.getHours(),
				   minutes: newFormatDate.getMinutes(),
           		};
               for ( var key in newDate){
                 if (newDate[key] < 10){
                     newDate[key] =  "0" + newDate[key];
                 } else {
                     newDate[key];
                 }
               }
          return newDate.year + "/" + newDate.month + "/" + newDate.day + " " + newDate.hours + ":" + newDate.minutes;
}

function editParams (carParam) {
	return carParam.status + "=>" + carParam.progress;
}

	
function editNewArr(newList){
	return newList.map(function(item){
		return {
				url:   		  editUrl(item.url),
                name:         editName(item.name),
				params:       editParams(item.params),
				description:  editDesc(item.description),
				date:         editDate(item.date)
		}
	})
}

function print(newArr){
  console.log(newArr);
}

function transform() {
    var count = 7;
    var newListCar = leftId (data, count)
    var editListCar = editNewArr(newListCar);
    print(editListCar);
}

btn.addEventListener("click", transform);