//Tabs

var tabLinks = document.getElementsByClassName("tab-link");
var tabContent = document.getElementsByClassName("tab-content");
var activeIndex = 0;

function initClick (array){
	for (var i = 0; i < array.length; i++){
		var link = array[i];
		handleClick(link, i);
	}
}
function handleClick (link, index){
	link.addEventListener('click', function(event) {
		event.preventDefault();
		goToTab(index);
	});
}
function goToTab (index){
	tabLinks[activeIndex].classList.remove('is-active');
	tabContent[activeIndex].classList.remove('is-active');
	tabLinks[index].classList.add('is-active');
	tabContent[index].classList.add('is-active');
	activeIndex = index;
}
initClick (tabLinks);

//Counter

var inputText = document.querySelector("#gettext");
var setText = document.querySelector("#settext");
var countLetters = document.querySelector("#count-letters");
var resetText = document.querySelector("#reset");

function countSymbols (event) {
	var textLength = inputText.value.length;
	countLetters.innerHTML = textLength;
	if (textLength < 200) {
		countLetters.classList.remove("warning");

	} else {
		countLetters.classList.add("warning");
		inputText.value = inputText.value.substring(0, 199);
	}
	setText.value = inputText.value;
}
function resetContainer (){
	countLetters.innerHTML = 0;
	setText.value = "";
	inputText.value = '';
	countLetters.classList.remove("warning");
}

inputText.addEventListener("input", countSymbols);
resetText.addEventListener("click", resetContainer);

//Gallery


var addImage = document.querySelector("#add");
var galleryArea = document.querySelector("#result");
var countImages = document.querySelector("#count");
var editData = transformeArr(data);
var amountImages = 0;

function capitalizeFirstLetter(word){
	return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
}
function addProtocol(adress){
	return "http://" + adress;
}
function truncateString(string){
	return string.substring(0, 15) + "...";
}
function formatDate(date){
	var tmpDate = new Date(date);
	var convertedDate = {
		year: tmpDate.getFullYear(),
		month: tmpDate.getMonth() + 1,
		date: tmpDate.getDate(),
		hours: tmpDate.getHours(),
		minutes: tmpDate.getMinutes()
	};
	for (var key in convertedDate){
		convertedDate[key] = (convertedDate[key] < 10) ? "0" + convertedDate[key] : convertedDate[key];
	}
	return convertedDate.year + "/" + convertedDate.month + "/" + convertedDate.date + " " + convertedDate.hours + ":" +
			convertedDate.minutes;
}
function editParams(firstPar, secondPar){
	return String(firstPar) + " => " + String(secondPar);
}
function transformeArr(arr){
	return arr.map(function(item){
		return {
			name: capitalizeFirstLetter(item.name),
			url: addProtocol(item.url),
			description: truncateString(item.description),
			date: formatDate(item.date),
			params: editParams(item.params.status, item.params.progress)
		};
	});
}

function printWithInterpolation(item, row){
		var resultHTML = `<div class="col-sm-3 col-xs-6 item-wrapper">\
	<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
	<div class="info-wrapper">\
	<div class="text-muted">${item.name}</div>\
	<div class="text-muted">${item.description}</div>\
	<div class="text-muted">${item.params}</div>\
	<div class="text-muted">${item.date}</div>\
	</div>\
	<button class="btn btn-danger">Удалить</button>\
	<div>`;
	row.innerHTML += resultHTML;
}
function printElementInGallery(){
	if (amountImages < editData.length) {
		printWithInterpolation(editData[amountImages], galleryArea);
		amountImages++;
		countImages.innerHTML = amountImages;
	}
	else {
		alert("Больше элементов нет!");
	}
}
function removeElementFromGallery(event){
	var galElement = event.target.closest('.item-wrapper');
	galleryArea.removeChild(galElement);
	amountImages = amountImages -1;
	countImages.innerHTML = amountImages;
}

addImage.addEventListener("click", printElementInGallery);
galleryArea.addEventListener("click", removeElementFromGallery);